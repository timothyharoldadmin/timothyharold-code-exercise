import Vue from 'vue'
import { shallowMount } from '@vue/test-utils'
import CompJumbotron from '@/components/CompJumbotron'

describe('CompJumbotron.vue', () => {
  const wrapper = shallowMount(CompJumbotron, { dataData: { pageTitle: 'Summit' } })
  const vm = wrapper.vm
  it('renders page title correctly', () => {
    Vue.nextTick(() => {
      expect(vm.$el.querySelector('.atlassian-page-title-container h1').textContent).toBe('Summit')
    })
  })
  it('renders section title correctly', () => {
    Vue.nextTick(() => {
      expect(vm.$el.querySelector('.section-title-container p').textContent).toBe('Video Archive')
    })
  })
})
