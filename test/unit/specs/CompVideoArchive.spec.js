import Vue from 'vue'
import { mount } from '@vue/test-utils'
import CompVideoArchive from '@/components/CompVideoArchive'

describe('CompVideoArchive.vue', () => {
  const wrapper = mount(CompVideoArchive)
  const vm = wrapper.vm
  it('loads and stores the json data for content', () => {
    Vue.nextTick(() => {
      expect(vm.jsonData.length).not.toHaveLength(0)
    })
  })
  it('stores tracks from the json data', () => {
    Vue.nextTick(() => {
      expect(vm.tracks.length).not.toHaveLength(0)
    })
  })
  it('stores sessions for the default track', () => {
    Vue.nextTick(() => {
      expect(vm.sessionsForSelectedTrack.length).not.toHaveLength(0)
    })
  })
  it('stores selectedSessionId for the default session', () => {
    Vue.nextTick(() => {
      expect(vm.selectedSessionId).not.toEqual('')
    })
  })
  it('stores selectedSession for the default session', () => {
    Vue.nextTick(() => {
      expect(vm.selectedSession).objectContaining({
        Id: expect.any(String),
        Title: expect.any(String),
        Track: expect.any(Object),
        Speakers: expect.any(Array)
      })
    })
  })
})
