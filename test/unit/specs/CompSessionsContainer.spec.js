import Vue from 'vue'
import { mount } from '@vue/test-utils'
import CompSessionsContainer from '@/components/CompSessionsContainer'

const propsData = {
  trackTitle: 'Build',
  sessionId: '27829',
  sessions: [
    {
      'Room': {
        'Name': 'Golden Gate B',
        'Capacity': '575'
      },
      'Id': '27829',
      'Description': 'Tractors, sprayers, and combines have more technology than you think – and they\'re getting smarter every day. Join this session to learn how cutting-edge technology is being used in a manufacturing company. Diego Lucas will walk through AGCO\'s three-year journey from a traditional, ad-hoc set of technologies and processes for the development lifecycle to a modern, agile automation-obsessed devops culture, using Atlassian tools in conjunction with technologies such as Heroku and Docker. He\'ll also discuss cultural aspects of change as well as go into lessons learned and provide a glimpse of what lies ahead.',
      'IsFeatured': 'False',
      'TimeSlot': {
        'EndTime': '11/5/2015 7:35:00 PM',
        'Label': 'Breakout 7',
        'StartTime': '11/5/2015 6:50:00 PM'
      },
      'Track': {
        'Title': 'Build',
        'Description': 'Build'
      },
      'LastModified': '9/22/2015 4:07:06 AM',
      'Published': 'True',
      'SessionType': {
        'Name': 'Breakout'
      },
      'MetadataValues': [
        {
          'Value': 'Basic',
          'Details': {
            'Title': 'Technical Level ',
            'Options': [
              'Basic',
              'Intermediate',
              'Advanced'
            ],
            'FieldType': 'DropDown'
          }
        }
      ],
      'Title': 'Tractors and DevOps: Harvesting the Fruits of Automation',
      'Speakers': [
        {
          'AttendeeID': 'bMedMbpxRnZpdXhYv7y8',
          'Biography': '',
          'LastName': 'Lucas',
          'EmailAddress': '',
          'Title': '',
          'AvailableforMeeting': 'False',
          'FirstName': 'Diogo',
          'Industry': '',
          'Roles': [
            'Speaker'
          ],
          'Company': 'AGCO',
          'LastUpdated': '9/22/2015 4:07:06 AM',
          'ID': '111417'
        }
      ],
      'Mandatory': 'False'
    },
    {
      'Room': {
        'Name': 'Golden Gate B',
        'Capacity': '575'
      },
      'Id': '28498',
      'Description': 'Atlassian faces the same issues as any other software company in the world. The battle for continuous integration glory is fought every day, and at stake is nothing less than our development and delivery speed. Join us to find out how we do it at Atlassian, powered by Bamboo. Because in the Game of Codes, you win... or you die.',
      'IsFeatured': 'False',
      'TimeSlot': {
        'EndTime': '11/4/2015 11:15:00 PM',
        'Label': 'Breakout 5',
        'StartTime': '11/4/2015 10:45:00 PM'
      },
      'Track': {
        'Title': 'Build',
        'Description': 'Build'
      },
      'LastModified': '9/22/2015 4:07:06 AM',
      'Published': 'True',
      'SessionType': {
        'Name': 'Breakout'
      },
      'MetadataValues': [
        {
          'Value': 'Intermediate',
          'Details': {
            'Title': 'Technical Level ',
            'Options': [
              'Basic',
              'Intermediate',
              'Advanced'
            ],
            'FieldType': 'DropDown'
          }
        }
      ],
      'Title': 'Game of Codes: the Battle for CI',
      'Speakers': [
        {
          'AttendeeID': 'aQtiTasZoedQOQkayhVe',
          'Biography': 'I was born in Spain, and spent almost all my life there enjoying the relaxing lifestyle of my beautiful Andalusia. I decided to move to London due to work, and while I was there Atlassian came on a bus to Steal 15 Developers in 15 Days, one of our most successful recruitment campaigns ever. Lucky me, I was one of them. I got on the bus and moved to Sydney with my husband, around 3 years ago - where I have been enjoying the relaxing Aussie lifestyle. I guess it\'s normal I feel like home here.\r\nI am a Bamboo developer, and I am passionate about CI and CD, and about collaboration in general when building software.',
          'LastName': 'Asenjo',
          'Interests': 'Photography, Street Art, Interior Design.',
          'EmailAddress': '',
          'Title': 'Software Developer',
          'AvailableforMeeting': 'True',
          'FirstName': 'Esther',
          'Roles': [
            'Speaker'
          ],
          'Twitter': 'https://twitter.com/Sthreo',
          'LinkedIn': 'https://www.linkedin.com/in/easenjo',
          'Company': 'Atlassian',
          'PhotoLink': 'https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/113702',
          'LastUpdated': '9/22/2015 4:07:06 AM',
          'ID': '113702'
        }
      ],
      'Mandatory': 'False'
    }
  ]
}

describe('CompSessionsContainer.vue', () => {
  const wrapper = mount(CompSessionsContainer, { propsData: propsData })
  const vm = wrapper.vm
  it('renders track title correctly', () => {
    Vue.nextTick(() => {
      expect(vm.$el.querySelector('h1.comp-sessions-header-title').textContent).toBe('Build')
    })
  })
  it('sessionId matches selected id: 27829', () => {
    Vue.nextTick(() => {
      expect(vm.$el.querySelector('.section-title-container p').textContent).toBe('27829')
    })
  })
  it('sessions array contains minimum 1 session', () => {
    Vue.nextTick(() => {
      expect(vm.sessions).toBeGreaterThan(0)
    })
  })
})
