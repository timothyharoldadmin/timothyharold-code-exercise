import Vue from 'vue'
import Router from 'vue-router'
import CompVideoArchive from '@/components/CompVideoArchive'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'CompVideoArchive',
      component: CompVideoArchive
    }
  ]
})
