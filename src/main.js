// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// use where needed: /modules/EventBus

import CompNavBar from '@/components/CompNavBar'
import CompVideoArchive from '@/components/CompVideoArchive'
import CompJumbotron from '@/components/CompJumbotron'
import CompTracksNav from '@/components/CompTracksNav'
import CompSessionsContainer from '@/components/CompSessionsContainer'
import CompSession from '@/components/CompSession'
import CompSessionDetail from '@/components/CompSessionDetail'

// components for the 'page' within the SPA
Vue.component('comp-nav-bar', CompNavBar) // the top nav bar w/ atlassian logo on the left
Vue.component('comp-video-archive', CompVideoArchive) // the 'page' containing all goodies below the CompNavBar
Vue.component('comp-jumbotron', CompJumbotron) // top 'page' lockup, atlassian logo and section title
Vue.component('comp-tracks-nav', CompTracksNav) // the tabbed nav for the Tracks
Vue.component('comp-sessions-container', CompSessionsContainer) // to contain the session per selected track
Vue.component('comp-session', CompSession) // the session 'tile' in the CompSessionsContainer scroller
Vue.component('comp-session-detail', CompSessionDetail) // the session details to the right of the CompSessionsContainer

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
