# timothyharold-code-exercise,  May 27, 2018
---
code exercise for Atlassian

[Live Demo](http://timothyharold.com/a-c-e)
Code is in the 'master' Branch

[Live Demo Bonus Dynamic Route Matching](http://timothyharold.com/a-c-e-router)
Code is in the 'dyn-render' Branch

### NOTES
I am using the CompVideoArchive.vue component as the model for this example SPA in the interest of saving time. Normally, I would set up a proper store using Vuex.

There were inconsistencies in the PNG that I decided to build into the site. In the top of the session detail, the speaker name AND company are both in bold whereas the speaker name is in bold and the company is not at the bottom. Was this a test of ability to implement fine details? If not, this is the reasoning for the inconsistency in my example site.

-Technologies used for this example: HTML/CSS/SCSS/JS, Vue JS , Jest (including @vue/test-utils)

-vue cli was used to scaffold out the codebase

-Reviewed and incorporated information found at:

https://atlassian.design/guidelines/product
https://atlassian.design/guidelines/product/foundations/color
https://atlassian.design/guidelines/product/foundations/typography

-Used svg logos found at:

https://www.atlassian.com/company/news/press-kit

All code for the exercise can be found in the 'master' branch of the repository.
Code for the Bonus Dynamic Route Matching can be found in the 'dyn-render' Branch

### _Per the basic requirements:_

1) **DONE** - Used the JSON data for all content, static text used for page/section titles, the JSON is loaded into the CompVideoArchive.vue component

2) **DONE** - clicking Track tabs loads sessions from the JSON data for the selected Track, no page reload/refresh

3) **DONE** - Sessions for the selected Track tab are populated in the left side scroll box, clicking on a session loads its details into the right side details component

4) **DONE** - No page reload when clicking on the sessions in the left side 'nav'

### _And for the bonus points:_

1) **DONE** - _"Change the URL based on the current track and session combination -- URLs should load as expected when navigated to directly (loading the correct track and session)"_

/ - loads the dafault Tracks[0] and Sessions[0] (as /track/Build/session/27829)

/track/:trackid - loads the supplied Track and session[0] for this Track (as /track/Build/session/27829)

/track/:trackid/session/:sessionid - loads supplied Track and Session as /track/Build/session/27829

Code is in the 'dyn-render' Branch

[Live Demo Bonus Dynamic Route Matching](http://timothyharold.com/a-c-e-router)

2) **DONE** - _"Make it responsive"_
The example is responsive, limited to < 576 break point and above to limit timing, normally i would lean on Bootstrap or Material or <insert your favorite lib>  using their built in grid, spacers, and layout helpers, but I went vanilla this time out to avoid bloat and save a little time

_This was fun challenge!_

### Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ npm run dev

# build for production
$ npm run build

# run unit tests
$ npm run unit
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

---

### Directions for the  Atlassian Web Developer Code Exercise

##### What do I need to do?

The task is relatively straightforward: build a simple site using the provided screenshots and data.

The point of this exercise is to get a feel for how you approach HTML, CSS, and JavaScript.

The [Atlassian Design Guidelines](https://atlassian.design) will be helpful for any assets and color values you might need, and the site content can be found in the included JSON file.

Feel free to use any libraries or frameworks you're comfortable with.

In terms of time, our general guidance (and what we hope seems fair to you too) is that you shouldn't spend more than a few hours putting things together. If you're feeling crunched, it's better to provide a written description or outline of how you'd approach something than to push yourself to get things into code.

Good luck!

##### Basic requirements

- Use the JSON data for the majority of the content and use static text where it makes sense
- Each 'track' should have its own tab, and the tabs should be functional (a page reload is OK when switching tabs)
- The left side nav items are session titles; they should be different on each tab since there are different sessions for each track
- Clicking a left side nav item should cause the content to change without a page reload

##### Bonus points

*Don't stress on this part. If you've already spent a decent amount of time on the basic requirements, then a brief explanation of how you might handle this is absolutely fine. We mean it!*

- Change the URL based on the current track and session combination -- URLs should load as expected when navigated to directly (loading the correct track and session)
- Make it responsive -- use your own judgment how to deal with the horizontal nav

##### Delivery

- Create a public [Bitbucket](https://bitbucket.org) repository and send the repository URL to your recruiter.
